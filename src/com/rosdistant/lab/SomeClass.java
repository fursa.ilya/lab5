package com.rosdistant.lab;

import java.util.Objects;

//задание 1 из лабы 5. Реализовать класс и переопределить методы equals, hashCode, toString
public class SomeClass {
    private byte b;
    private int i;
    private double d;
    private String s;

    public SomeClass(byte b, int i, double d, String s) {
        this.b = b;
        this.i = i;
        this.d = d;
        this.s = s;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        SomeClass someClass = (SomeClass) o;
        return b == someClass.b &&
                i == someClass.i &&
                Double.compare(someClass.d, d) == 0 &&
                Objects.equals(s, someClass.s);
    }

    @Override
    public int hashCode() {
        return Objects.hash(b, i, d, s);
    }

    @Override
    public String toString() {
        return "SomeClass{" +
                "b=" + b +
                ", i=" + i +
                ", d=" + d +
                ", s='" + s + '\'' +
                '}';
    }
}
