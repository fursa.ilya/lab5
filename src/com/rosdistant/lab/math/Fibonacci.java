package com.rosdistant.lab.math;

import java.util.Scanner;

//Выводим числа фибоначи до числа введенного с клавиатуры
public class Fibonacci {

    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        System.out.print("Введите границу: ");
        int n = sc.nextInt();
        for (int i = 0; i < n; i++) {
            System.out.print(f(i) + " ");
        }
    }

    //Рекурсивное вычисление чисел фибоначчи(Каждое число - сумма двух предыдущих)
    public static int f(int n) {
        if (n == 0) {
            return 0;
        } else if (n == 1) {
            return 1;
        } else {
            return f(n - 1) + f(n - 2);
        }
    }
}
