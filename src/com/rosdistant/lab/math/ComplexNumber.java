package com.rosdistant.lab.math;

public class ComplexNumber {
    private double realPart; //действительная часть
    private double imgPart; //мнимая единица

    public ComplexNumber(double realPart, double imgPart) {
        this.realPart = realPart;
        this.imgPart = imgPart;
    }

    public double getImgPart() {
        return imgPart;
    }

    public double getRealPart() {
        return realPart;
    }

    public void print() {
        System.out.println("R: " + realPart + " I: " + imgPart);
    }
}
