package com.rosdistant.lab.math;

public class PascalTriangle {
    private int bound;
    private long matrix[][];

    public PascalTriangle(int bound) {
       this.bound = bound;
       matrix = new long[bound + 1][bound + 1];
       for (int i = 0; i <= bound; i++) {
           matrix[i][0] = matrix[i][i] = 1;

           for (int j = 1; j < i; j++) {
               matrix[i][j] = matrix[i - 1][j - 1] + matrix[i - 1][j];
           }
        }
    }

    public void displayTriangle() {
        for (int i = 0; i <= bound; i++) {
            for (int j = 0; j <= i; j++) {
                System.out.print(matrix[i][j] + " ");
            }

            System.out.println();
        }
    }

    public static void main(String[] args) {
        PascalTriangle pascalTriangle = new PascalTriangle(5);
        pascalTriangle.displayTriangle();
    }
}
