package com.rosdistant.lab;

public class Main {

    public static void main(String[] args) {
        //Создаем наши объекты и тестируем их
        Phone alexPhone = new Phone("Alex", "Petrov", 389213089);
        Phone peterPhone = new Phone("Peter", "Sidorov", 389120312);

        //Тестируем методы объектов equals, hashCode, toString.
        System.out.println(alexPhone.toString());
        System.out.println(peterPhone.toString());
        System.out.println(alexPhone.equals(peterPhone));
        System.out.println(alexPhone.hashCode());
        System.out.println(peterPhone.hashCode());
    }
}
